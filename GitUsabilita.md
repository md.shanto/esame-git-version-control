git init: Inizializza un nuovo repository Git nella directory corrente.

Usabilità: Utilizzato una sola volta all'inizio di un nuovo progetto per creare un repository Git.
git clone <repository>: Clona un repository Git esistente nella tua macchina locale.

Usabilità: Utilizzato per creare una copia locale di un repository remoto.
git add <file>: Aggiunge un file specifico o tutte le modifiche dei file alla staging area.

Usabilità: Utilizzato per preparare i file per l'esecuzione di un commit.
git commit -m "messaggio del commit": Esegue un commit delle modifiche presenti nella staging area.

Usabilità: Utilizzato per creare un punto di salvataggio delle modifiche nel repository.
git status: Mostra lo stato corrente del repository, inclusi i file modificati, quelli nella staging area e quelli non tracciati.

Usabilità: Utilizzato per monitorare le modifiche nel repository.
git log: Visualizza la cronologia dei commit effettuati nel repository.

Usabilità: Utilizzato per visualizzare i dettagli dei commit precedenti, come autore, data e messaggio.
git branch: Mostra l'elenco dei branch presenti nel repository.

Usabilità: Utilizzato per visualizzare i branch esistenti e per creare, rinominare o eliminare branch.
git checkout <branch>: Cambia il branch corrente in un altro specificato.

Usabilità: Utilizzato per passare da un branch all'altro nel repository.
git merge <branch>: Combina le modifiche presenti in un altro branch nel branch corrente.

Usabilità: Utilizzato per incorporare le modifiche di un branch in un altro.
git remote add <nome> <url>: Aggiunge un repository remoto con il nome specificato e l'URL associato.

Usabilità: Utilizzato per collegare un repository locale a un repository remoto.
git push <remote> <branch>: Carica i commit locali su un repository remoto specificato.

Usabilità: Utilizzato per inviare le modifiche locali al repository remoto.
git pull <remote> <branch>: Aggiorna il repository locale con le modifiche più recenti dal repository remoto specificato.

Usabilità: Utilizzato per sincronizzare il repository locale con il repository remoto.

git reset <commit>: Annulla i commit successivi al commit specificato, mantenendo le modifiche nei file.

Usabilità: Utilizzato per annullare i commit e ripristinare il repository a uno stato precedente.
git revert <commit>: Crea un nuovo commit che annulla le modifiche apportate dal commit specificato.

Usabilità: Utilizzato per annullare le modifiche di un commit senza eliminare la sua storia.
git rebase <branch>: Applica i commit del branch specificato sopra al branch corrente.

Usabilità: Utilizzato per riorganizzare la cronologia dei commit e integrare le modifiche di un branch in un altro in modo lineare.
git stash: Salva le modifiche locali in uno stash temporaneo.

Usabilità: Utilizzato per mettere da parte le modifiche non commesse in modo da poter passare a un altro branch senza commit.
git cherry-pick <commit>: Applica le modifiche di un commit specificato in un altro branch.

Usabilità: Utilizzato per selezionare e applicare selettivamente un singolo commit in un branch diverso.
git bisect: Esegue una ricerca binaria per individuare il commit che ha introdotto un bug o un problema.

Usabilità: Utilizzato per individuare la causa di un problema nel repository eseguendo una ricerca sistematica.
git blame <file>: Mostra l'ultimo commit e l'autore di ogni linea di un file.

Usabilità: Utilizzato per identificare quale commit ha introdotto una specifica modifica o problema in un file.

git submodule: Gestisce i sotto-moduli Git all'interno di un repository principale.

Usabilità: Utilizzato per includere repository Git esterni all'interno di un progetto.

git clean -n: Mostra un elenco dei file non tracciati che verrebbero rimossi utilizzando git clean.

Usabilità: Utilizzato per visualizzare in anteprima i file non tracciati che verrebbero eliminati durante un'operazione di pulizia.
